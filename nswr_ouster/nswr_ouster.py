import rclpy
from rclpy.node import Node
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs_py.point_cloud2 as pc2
from std_msgs.msg import Header
from std_msgs.msg import String
import math
import numpy as np
import cv2

class Ouster(Node):

    def __init__(self):
        super().__init__('nswr_ouster')

        # Subcribe data from the scanner
        self.subscription = self.create_subscription(PointCloud2,'/os_cloud_node/points',self.ouster_callback,1)

        # Fields in PointCloud2 ROS message for velodyne
        self.ouster_fields = [
            PointField(name='x',
                       offset=0,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='y',
                       offset=4,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='z',
                       offset=8,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='intensity',
                       offset=12,
                       datatype=PointField.UINT32,
                       count=1),
        ]

    def ouster_callback(self, msg):

        # Converting ROS message to point list
        points = pc2.read_points_list(msg,["x", "y", "z", "intensity"])

        SCANNER_COLS = 1024
        SCANNER_ROWS = 128
        MAX_V_ANGLE = 22.5
        MIN_V_ANGLE = -22.5

        intensity_img = np.zeros((SCANNER_ROWS,SCANNER_COLS,1), np.uint8)
        range_img = np.zeros((SCANNER_ROWS,SCANNER_COLS,1), np.uint8)

        for point in points:
            if np.isnan(point.x) or np.isnan(point.y) or np.isnan(point.z):
                continue

            # vert_angle = 
            # horiz_angle  = 

            # height = 
            # width = 

            if width >= SCANNER_COLS or width < 0 or height > SCANNER_ROWS or height < 0:
                continue

            # Create intensity image

        
            # Create range image

        
        # Normalize range_img to 0 - 255
        cv2.normalize(range_img, range_img, 0.0, 255.0, cv2.NORM_MINMAX, cv2.CV_8UC1)

        cv2.imshow("Intensity image",intensity_img)
        cv2.imshow("Range image",range_img)
        cv2.waitKey(50)

        # Save images to file
        #cv2.imwrite("", intensity_img);
        #cv2.imwrite("", range_img);


def main(args=None):
    
    rclpy.init(args=args)
    ouster = Ouster()

    rclpy.spin(ouster)

    ouster.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
